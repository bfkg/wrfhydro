## Description

wRfHydro is an extension package for R that provides easy and flexible pre- and postprocessing of data structures of the hydrologically enhanced version of the Weather Research and Forecasting model (WRF-Hydro) but also for the original WRF. WRF-Hydro is community developed modeling system and hosted at the National Center of Atmospheric Research (NCAR), Boulder CO.

The tool is useful for, e.g., data preprocessing (interpolation of non-standard dataset to the WRF grid), data extraction (time-series for a given coordinate), water budget analysis for a masked area of a WRF domain, and conversion of WRF-Output into WRF-Hydro stand-alone input. 

The package is based on RNetCDF which means only netdf 3 can be written. Netcdf 4 is supported for reading. Furhtermore, it builds on the sp and raster packages for its spatial functionalities. 

## Installation

### Using devtools package

> install_bitbucket(bfkg/wrfhydro.git)

### Clone and install from the shell

> git clone https://user@bitbucket.org/bfkg/wrfhydro.git

(replace user with your bitbucket username)

> cd wRfHydro

> R CMD build

> R CMD INSTALL wRfHydro_...tar.gz

(for global installation)

> sudo R CMD INSTALL 

### Dependencies
wRfHydro requires the following packages:

RNetCDF, plyr, raster, sp

For map plotting, ggplot2, reshape2, rgdal are recommended

