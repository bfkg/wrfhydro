#' @export

setGeneric("createNoahMpParms", function(ivgtyp, ...) standardGeneric("createNoahMpParms"))
setMethod("createNoahMpParms", c("WrfData2d"),
	    function(ivgtyp, mptable="", type="usgs"){
	      
	          
	          mpt<-readLines(mptable)
	          sidusgs<-grep("noahmp_usgs_veg", mpt)
	          sidmodis<-grep("noahmp_modis_veg", mpt)
	      
	          if(type=="usgs") mpt <-  mpt[sidusgs:sidmodis-1]
	          if(type=="modis")  mpt <- mpt[sidmodis:length(mpt)]
	          
	          # remove fortran comments
	          mpt<-mpt[-grep("!", mpt)]
	          
	          
	          
	          cwpvt_tbl <- as.numeric(strsplit(strsplit(mpt[grep("CWPVT", mpt)], "=")[[1]][2], ",")[[1]])
	          cwpvt <- ivgtyp
              cwpvt@data<-matrix(mapply(function(x) cwpvt_tbl[x], cwpvt@data), nrow=nrow(cwpvt@data), ncol=ncol(cwpvt@data))
	          cwpvt@varname="cwpvt"
	          
	          vcmx25_tbl <- as.numeric(strsplit(strsplit(mpt[grep("VCMX25", mpt)], "=")[[1]][2], ",")[[1]])
	          vcmx25 <- ivgtyp
              vcmx25@data<-matrix(mapply(function(x) vcmx25_tbl[x], vcmx25@data), nrow=nrow(vcmx25@data), ncol=ncol(vcmx25@data))
	          vcmx25@varname="vcmx25"
	          
	          mp_tbl <- as.numeric(strsplit(strsplit(mpt[grep("MP", mpt)], "=")[[1]][2], ",")[[1]])
	          mp <- ivgtyp
              mp@data<-matrix(mapply(function(x) mp_tbl[x], mp@data), nrow=nrow(mp@data), ncol=ncol(mp@data))
	          mp@varname="mp"
              
	          hvt_tbl <- as.numeric(strsplit(strsplit(mpt[grep("HVT", mpt)], "=")[[1]][2], ",")[[1]])
	          hvt <- ivgtyp
              hvt@data<-matrix(mapply(function(x) hvt_tbl[x], hvt@data), nrow=nrow(hvt@data), ncol=ncol(hvt@data))
	          hvt@varname="hvt"
	          
 	          mfsno_tbl <- as.numeric(strsplit(strsplit(mpt[grep("MFSNO", mpt)], "=")[[1]][2], ",")[[1]])
	          mfsno <- ivgtyp
              mfsno@data<-matrix(mapply(function(x) mfsno_tbl[x], mfsno@data), nrow=nrow(mfsno@data), ncol=ncol(mfsno@data))
	          mfsno@varname="mfsno"
	          
	          
             return(list(cwpvt=cwpvt, vcmx25=vcmx25, mp=mp, hvt=hvt, mfsno=mfsno))         
              
	      
	      }
	  )
	  

#   REAL, ALLOCATABLE, DIMENSION(:,:)       ::  cwpvt_2D   ! Canopy wind parameter
#   REAL, ALLOCATABLE, DIMENSION(:,:)       ::  vcmx25_2D  ! VCmax at 25C
#   REAL, ALLOCATABLE, DIMENSION(:,:)       ::  mp_2D      ! Slope of Ball-Berry rs-P relationship
#   REAL, ALLOCATABLE, DIMENSION(:,:)       ::  hvt_2D     ! Canopy Height
#   REAL, ALLOCATABLE, DIMENSION(:,:)       ::  mfsno_2D   ! Snow cover m parameter
