setClass("pp.daily",
           representation(files="list"),
	   prototype(     files=vector(mode="list")
	  
	  )
)

setClass("pp.monthly",
           representation(files="list"),
	   prototype(     files=vector(mode="list")
	  
	  )
)

setClass("yearly",
           representation(files="list"),
	   prototype(     files=vector(mode="list")
	  
	  )
)


setMethod("head", c("Sim"),
	  function(x, n=10){
	    x@files <- lapply(x@files, "[", 1:n)
	    x@times <- x@times[1:n]
	    x
	  }
)

setMethod("tail", c("Sim"),
	  function(x, n=10){
	    x@files <- lapply(x@files, "[", (length(x@files[[1]])-n+1):(length(x@files[[1]])))
	    x
	  }
)

#' @importFrom methods new
#' @export
setGeneric("downstrmProfile", function(wrf2d, cgrd, fdir, scrd)standardGeneric("downstrmProfile"))
setMethod("downstrmProfile", c("WrfData2d","WrfData2d","WrfData2d", "numeric"),
	  function(wrf2d, cgrd, fdir, scrd){

            dat <- wrf2d@data
            cgrd <- cgrd@data
            fdir <- fdir@data
            len<-0
            lenv<-NULL
	    val <- cx <- cy <- gp <- NULL
	    
	    ncrd <- scrd
	    
	    hold <- dat[ncrd[1],ncrd[2]]
	    
	    for(i in 1:10000) {
	     
	     val[i]<-dat[ncrd[1],ncrd[2]]
	     cx[i] <-ncrd[1]
	     cy[i] <-ncrd[2]
	     
 	     hold<-val[i]
 	    
#  	     #check if we have a gauge point
#              if(is.na(gauge[ncrd[1],ncrd[2]])) gp[i] <- NA
#  	     if(!is.na(gauge[ncrd[1],ncrd[2]])) gp[i] <- i
 	    

 	     if(abs(fdir[ncrd[1],ncrd[2]])==1)   {ncrd <- ncrd+c( 1, 0); len <- len + wrf2d@dx}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==2)   {ncrd <- ncrd+c( 1,-1); len <- len + sqrt(2)*mean(wrf2d@dx, wrf2d@dy)}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==4)   {ncrd <- ncrd+c( 0,-1); len <- len + wrf2d@dy}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==8)   {ncrd <- ncrd+c(-1,-1); len <- len + sqrt(2)*mean(wrf2d@dx, wrf2d@dy)}
             if(abs(fdir[ncrd[1],ncrd[2]])==16)  {ncrd <- ncrd+c(-1, 0); len <- len + wrf2d@dx}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==32)  {ncrd <- ncrd+c(-1, 1); len <- len + sqrt(2)*mean(wrf2d@dx, wrf2d@dy)}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==64)  {ncrd <- ncrd+c( 0, 1); len <- len + wrf2d@dy}
 	     if(abs(fdir[ncrd[1],ncrd[2]])==128) {ncrd <- ncrd+c( 1, 1); len <- len + sqrt(2)*mean(wrf2d@dx, wrf2d@dy)}
 	    
	     lenv[i] <- len
 	     if(ncrd[1]>dim(dat)[1]||ncrd[2]>dim(dat)[2]) break
             if(is.na(cgrd[ncrd[1],ncrd[2]])) stop("We've reached a non-channel cell. Stopping.")
	      
	    }

	    hcs <- new("HcSect")
	    hcs@value <-val
	    hcs@scoord<-scrd
	    hcs@ecoord<-ncrd
	    hcs@len<-lenv
	    hcs@name<-wrf2d@varname
	    
	    hcs
	    
	  }
)

# create daily statistics from hourly data
#' @export
setGeneric("pp.daily", function(flist, outfile.type, varname, refWrf2d,...) standardGeneric("pp.daily"))
setMethod("pp.daily", c("Sim","numeric","character","WrfData2d"),
           function(flist, outfile.type, varname, refWrf2d, cumulative=FALSE, fun="mean", dims=2, missval=NA, 
		    ignore.incomplete=TRUE, parallel=FALSE, write2nc=FALSE, out.path="", out.varname=NULL,
	            longname="NA", uvar=" ", dtype="double"){

	  if(is.null(out.varname)) out.varname <- varname
          if(outfile.type==1) times<-strptime(unlist(lapply(strsplit(flist@files[[outfile.type]],"/"),"tail",1)), format="%Y%m%d%H")
          if(outfile.type >1) times<-strptime(unlist(lapply(strsplit(flist@files[[outfile.type]],"/"),"tail",1)), format="%Y%m%d%H%M")

          # daily aggregation
          d.times<-format(times,"%Y%m%d")
          days  <-unique(d.times)
          idx.d<-vector(mode="list", length=length(days))
          for(i in 1:length(days)) idx.d[[i]]<-which(d.times==days[i])
           if(!cumulative){ # state variables
	    if(parallel){ # only works if parallel backend is registered (e.g. doSNOW, PVM, ...)
            require(foreach)
            var.agg.d <- foreach(i = 1:length(idx.d), .verbose=TRUE) %dopar%{
             require(wRfHydro)
	     if((!ignore.incomplete) || (length(idx.d[[i]])==24)) {
	       agg <- aggrField(flist@files[[outfile.type]][idx.d[[i]]], varname, fun=fun, missval=missval)
	     }else{
	       agg <- NA
	       write2nc=FALSE
	     }
	     if(write2nc){ # write netcdf output file
	       refWrf2d@varname<-varname
	       if(dims==2){
	        refWrf2d@data <- agg
                write.wrf2d(paste(out.path,"/",days[i],"_",out.varname,".nc", sep=""), refWrf2d , longname=longname, 
			    uvar=uvar, dtype=dtype, time=paste("Seconds since", strptime(days[i], format="%Y%m%d"), "12:00:00"))		 
	       }else if (dims==3){ # 3d fields, e.g. soil moisture		 
                 for(j in 1:dim(agg)[3]) { # write one file per layer
	          refWrf2d@data <- agg[,,j]
                  write.wrf2d(paste(out.path,"/",days[i],"_",out.varname,"_l",j,".nc", sep=""), refWrf2d , longname=longname, 
			      uvar=uvar, dtype=dtype, time=paste("Seconds since",strptime(days[i], format="%Y%m%d"), "12:00:00"))
	         }
               }	      
	     }else{ # return list of (all processed daily) fields to calling routine
              agg
	     }
            }

           }else{ # sequential

            if(!write2nc) agg <- vector(mode="list", length=length(idx.d))

            for(i in 1:length(idx.d)){
	     if((ignore.incomplete) && (length(idx.d[[i]])!=24)) next
	     if(write2nc){
	       refWrf2d@varname<-varname
	       agg <- aggrField(flist@files[[outfile.type]][idx.d[[i]]], varname, fun=fun, missval=missval)
	       if(dims==2){
	        refWrf2d@data <- agg
                write.wrf2d(paste(out.path,"/",days[i],"_",out.varname,".nc", sep=""), refWrf2d , longname=longname, 
			    uvar=uvar, dtype=dtype, time=paste("Seconds since",strptime(days[i], format="%Y%m%d"), "12:00:00"))		 
	       }else if (dims==3){ # 3d fields, e.g. soil moisture		 
                 for(j in 1:dim(agg)[3]) { # write one file per layer
	          refWrf2d@data <- agg[,,j]
                  write.wrf2d(paste(out.path,"/",days[i],"_",out.varname,"_l",j,".nc", sep=""), refWrf2d , longname=longname, 
			      uvar=uvar, dtype=dtype, time=paste("Seconds since",strptime(days[i], format="%Y%m%d"), "12:00:00"))
	         }
               }	      
	     }else
	        
	      if((ignore.incomplete) && (length(idx.d[[i]])!=24)){
		agg[[i]]<-NA
	      }else{
	      
               agg[[i]] <- aggrField(flist@files[[outfile.type]][idx.d[[i]]], varname, fun=fun, missval=missval)
	      }
	    }
	    agg # return list of daily aggregated fields
           } 

	   }else{ # cumulative (flux) variables
	     
	     
	   }

	   } # end function
)

#' @export
setGeneric("wrf2d.budget", function(wrf2d, msk, fun="sum")standardGeneric("wrf2d.budget"))
setMethod("wrf2d.budget", c("WrfData2d","WrfData2d"),
	    function(wrf2d, msk, fun){
	      norm<-sum(msk@data, na.rm=TRUE)
              x <- as.vector(wrf2d@data)*as.vector(msk@data)
              if(fun=="sum") x <-sum(x,na.rm=T)
              if(fun=="mean") x <-sum(x,na.rm=T)/norm
              x
	    }

)




