#' Import simulation results from WRF or WRf-Hydro simulation into R
#'
#' Reads meta information about a WRF or WRF-Hydro simulation including file names, simulation type (WRF stand-alone, WRF-Hydro coupled).  
#' Either one of whPath or wrfPath must be specified. Currently simulations can only be imported for a single domain.
#'
#' @rdname loadSim-methods
#' @param whPath Path to where WRF-Hydro output files are located (optional)
#' @param wrfPath Path to where WRF outpuf files are located (optional)
#' @param wrfStatic Path to where the static WRF input files (wrfinput_d0x) are located (optional)
#' @param hydroStatic Path to where the static WRF-Hydro input (e.g., hires.nc) file is located (optional)
#' @param lsm2dStatic Path to where the static LDAS spatial soil file is located (optional)
#' @param gw2dStatic Path to where gw2d static input file (e.g., gwhires.nc) is located (optional)
#' @param wrfOutPattern Prefix of the WRF output file names
#' @param timesImport Select method for time information import: valid options are "filename", "guess", or "full"
#' @param par How many processes to use in parallel
#' @param update Update file inventory database (reprocess files)
#' @param verbose Show file names currently being processed
#' @return Returns a simulation object with meta information.
#' @export
setGeneric("loadSim", function(...)standardGeneric("loadSim"))
#' @importFrom parallel mclapply
#' @importFrom digest digest
#' @rdname loadSim-methods
#' @aliases loadSim,ANY-method
setMethod("loadSim", "ANY",
	  function(whPath=NA, wrfPath=NA, wrfStatic="", hydroStatic="",
	           lsm2dStatic="", gw2dStatic="", wrfOutPattern="wrfout_d03",
	           timesImport="full", par=1, update=FALSE, verbose=FALSE){
	    tic <- Sys.time()
	    
	    if(!is.na(whPath))  stopifnot(file.exists(whPath))
	    
	    if(!is.na(wrfPath)) stopifnot(file.exists(wrfPath))
	    
	    hash <- digest(paste0(whPath, wrfPath, wrfStatic, hydroStatic, lsm2dStatic, gw2dStatic), "md5", serialize=FALSE)
	    if(verbose) print(paste("Simulation hash value: ", hash))
	    
	    # read from database instead of reimporting the simulation if database file exists
	    if(!update && file.exists(paste0("~/.wRfHydro/", hash))) {
	    
	       if(verbose) print("Reading simulation information from database")
	       load(paste0("~/.wRfHydro/", hash))
	       print(paste0("Imported simulation from database file for the period: ", sim@times[1], " to ", tail(sim@times, 1), "."))
	       return(sim)
	    }
	    
	    files <- vector(mode="list",4)
	    
	    if(is.na(wrfPath)){
	      files[[1]] <-  grep("[:.:]LDASOUT_DOMAIN",dir(whPath, full.names=T), value=T)
	      ldasout <- TRUE
	      nLsm <- length(files[[1]])
	      simTimes <- as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[1]], "/"), function(x) tail(x, n=1))), "[.]"), function(x) head(x, n=1))), format="%Y%m%d%H", tz="UTC"))
	      inventory <- data.frame(DATE=simTimes, ldasFileName=files[[1]], stringsAsFactors = FALSE)
	    }else{
	      files[[1]] <-  grep(wrfOutPattern,dir(wrfPath, full.names=T), value=T)
	      wrfout<-TRUE
	      nLsm <- length(files[[1]])

	      
     	   nc <- open.nc(files[[1]][1])
	       tms<-strptime(var.get.nc(nc, "Times"), "%Y-%m-%d_%H:%M:%S", tz="UTC")
	       dt<-difftime(tms[2], tms[1], units="mins")
	       close.nc(nc)
	       nc <- open.nc(tail(files[[1]],n=1))
	       tms2<-strptime(var.get.nc(nc, "Times"), "%Y-%m-%d_%H:%M:%S", tz="UTC")
	       close.nc(nc)

	      
	      if(timesImport=="filename"){
	       
	       if(length(tms)!=1) stop("TimesImport=filename only useful if only single times are in one file.")
	       simTimes<-as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[1]], "/"), function(x) tail(x, n=1))), "_"), function(x) paste(x[3],x[4],sep="_"))),format="%Y-%m-%d_%H:%M:%S", tz="UTC"))
	       
	       inventory <-data.frame(DATE=simTimes, wrfTimeindex=c(rep(1:length(tms), length(files[[1]])-1 ), 1:length(tms2)),
	                             wrfFileName=c(rep(head(files[[1]], n=-1), each=length(tms)), rep(tail(files[[1]], n=1), each=length(tms2))), stringsAsFactors = FALSE)
	      }else if(timesImport=="guess"){
	      
	      
	       # make wrf output files time inventory
	       
	       simTimes <- seq.POSIXt(tms[1], tail(tms2, n=1),  
	                     length=length(tms)*(length(files[[1]])-1)+length(tms2))
	       
	       inventory <-data.frame(DATE=simTimes, wrfTimeindex=c(rep(1:length(tms), length(files[[1]])-1 ), 1:length(tms2)),
	                             wrfFileName=c(rep(head(files[[1]], n=-1), each=length(tms)), rep(tail(files[[1]], n=1), each=length(tms2))), stringsAsFactors = FALSE)
	                             
	       }else if(timesImport=="full"){
	        # reads all timesteps from output files
	        inventory<-do.call(rbind, mclapply(as.list(files[[1]]), function(x) {
                                  if(verbose) cat(x, "\r");
	                              nc <- open.nc(x); 
	                              tms<-var.get.nc(nc, "Times");
	                              close.nc(nc); 
	                              data.frame(DATE=tms, wrfFileName=as.character(x), wrfTimeindex=1:length(tms),
	                              stringsAsFactors=FALSE)},
	                              mc.cores=par))
	        inventory$DATE <- as.POSIXct(strptime(inventory$DATE, "%Y-%m-%d_%H:%M:%S", tz="UTC"))
	        inventory<-inventory[!duplicated(inventory$DATE, fromLast=TRUE),]
            if(verbose) print("\n")
	       }else{
	        
	        stop(paste("Option",timesImport,"unknown for timesImport."))
	       
	       }
	        
	    }
	    if(!is.na(whPath)){
	     files[[2]] <-  grep("[:.:]RTOUT_DOMAIN",  dir(whPath, full.names=T), value=T)
	     nRtout <- length(files[[2]])
	     if(nRtout>0) inventory <-  merge(inventory, data.frame(DATE=as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[2]], "/"), function(x) tail(x, n=1))), "[.]"), function(x) head(x, n=1))), format="%Y%m%d%H", tz="UTC")),
	                                    rtFileName=files[[2]], stringsAsFactors = FALSE), by="DATE")
	     files[[3]] <-  grep("[:.:]CHRTOUT_DOMAIN",dir(whPath, full.names=T), value=T)
	     nChrtoutDom <- length(files[[3]])
	     if(nChrtoutDom>0) inventory <-  merge(inventory, data.frame(DATE=as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[3]], "/"), function(x) tail(x, n=1))), "[.]"), function(x) head(x, n=1))), format="%Y%m%d%H", tz="UTC")),
	                           chrtFileName=files[[3]], stringsAsFactors = FALSE), by="DATE")
	     files[[4]] <-  grep("[:.:]LSMOUT_DOMAIN", dir(whPath, full.names=T), value=T)
	     nLsmout <- length(files[[4]])
	     if(nLsmout>0) inventory <-  merge(inventory, data.frame(DATE=as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[4]], "/"), function(x) tail(x, n=1))), "[.]"), function(x) head(x, n=1))), format="%Y%m%d%H", tz="UTC")),
	                           lsmFileName=files[[4]], stringsAsFactors = FALSE), by="DATE")
	     files[[5]] <-  grep("[:.:]GW_DOMAIN", dir(whPath, full.names=T), value=T)
	     nGw <- length(files[[5]])
     	     if(nGw>0) inventory <-  merge(inventory, data.frame(DATE=as.POSIXct(strptime(unlist(lapply(strsplit(unlist(lapply(strsplit(files[[5]], "/"), function(x) tail(x, n=1))), "[.]"), function(x) head(x, n=1))), format="%Y%m%d%H", tz="UTC")),
                                  gwFileName=files[[5]], stringsAsFactors = FALSE), by="DATE")
           }else{
	     nGw<-0
	    }
	    
	    
            simTimes <- inventory[,1]
            
	    sim<-new("Sim")
	    
	    # determine simulation type
	    if(is.na(wrfPath) && !is.na(whPath)){
	      sim@type="whSa"
	      print(paste("Imported WRF-Hydro stand-alone simulation for the period", range(simTimes)[1], "to" ,range(simTimes)[2]))
	    }else if(!is.na(wrfPath) && !is.na(whPath)){
	      sim@type="whCpl"
	      print(paste("Imported WRF coupled WRF-Hydro simulation for the period", range(simTimes)[1], "to" ,range(simTimes)[2] ))
	    }else if(!is.na(wrfPath) && is.na(whPath)){
	      sim@type="wrfSa"
	      print(paste("Imported WRF stand-alone simulation for the period", range(simTimes)[1], "to" ,range(simTimes)[2] ))
	    }
	    
	    if(nGw > 1) {
	      sim@type<-paste(sim@type, "Gw2d", sep="")
	      print("2d groundwater model output detected.")
	      if(gw2dStatic=="") print("Warning: staticGw2d path not specified. whBalance() will fail for groundwater budget computations.")
	    }
	      
	    sim@files<-files
	    sim@staticWRF<-wrfStatic
	    sim@staticLSM<-lsm2dStatic
	    sim@staticHydro<-hydroStatic
	    sim@staticGw2d<-gw2dStatic
	    sim@times<-simTimes
	    sim@inventory<-inventory
	    
	    # save sim object to disk for later use
	    if(!dir.exists("~/.wRfHydro")) dir.create("~/.wRfHydro")
	    save(sim, file=paste0("~/.wRfHydro/", hash))
	    
	    toc <- Sys.time()
	    if(verbose) print(toc-tic)
	    sim
	   
	  }
)
